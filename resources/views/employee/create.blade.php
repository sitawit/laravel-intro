<!-- create.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="post" action="{{url('employee')}}">
            <div class="form-group row">
                {{csrf_field()}}
                <label for="lgFormFirstNameInput" class="col-sm-2 col-form-label col-form-label-lg">First Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-lg" id="lgFormFirstNameInput" placeholder="First Name..." name="first_name">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormLastNameInput" class="col-sm-2 col-form-label col-form-label-lg">Last Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-lg" id="lgFormLastNameInput" placeholder="Last Name..." name="last_name">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <input type="submit" class="btn btn-primary">
            </div>
        </form>
    </div>
@endsection