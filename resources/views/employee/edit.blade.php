<!-- create.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="post" action="{{action('EmployeeController@update', $id)}}">
            <div class="form-group row">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PATCH">
                <label for="lgFormFirstNameInput" class="col-sm-2 col-form-label col-form-label-lg">First Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-lg" id="lgFormFirstNameInput" placeholder="First Name..." name="first_name" value="{{$employee->first_name}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormLastNameInput" class="col-sm-2 col-form-label col-form-label-lg">Last Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control form-control-lg" id="lgFormLastNameInput" placeholder="Last Name..." name="last_name" value="{{$employee->last_name}}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
@endsection