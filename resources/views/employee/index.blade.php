<!-- index.blade.php -->
@extends('layouts.app')
@section('content')
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th colspan="2">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
                <tr>
                    <td>{{$employee['id']}}</td>
                    <td>{{$employee['first_name']}}</td>
                    <td>{{$employee['last_name']}}</td>
                    <td><a href="{{action('EmployeeController@edit', $employee['id'])}}" class="btn btn-warning">Edit</a></td>
                    <td>
                        <form action="{{action('EmployeeController@destroy', $employee['id'])}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{action('EmployeeController@create')}}" class="btn btn-primary btn-lg">Create</a>
    </div>
@endsection